#ifndef DELAY_H
#define DELAY_H

#include "stm8s_clk.h"
#include "stm8s_tim4.h"

void delay_us(uint32_t us);
void delay_ms(uint32_t ms);
void delay_us_check(uint32_t time);

#endif