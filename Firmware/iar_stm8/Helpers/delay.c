#include "delay.h"

#define TIM4_PERIOD       124
#define TIM4_US_PERIOD       4

volatile uint32_t count;

//
// IRQ_Handler_TIM4 IRQ handler
//
INTERRUPT_HANDLER(IRQ_Handler_TIM4, 23)
{
  if (count != 0)
    count--;
  
  TIM4_ClearITPendingBit(TIM4_IT_UPDATE);
}

//
// Delay im microseconds
//
void delay_us(uint32_t us)
{
  TIM4_Cmd(DISABLE);       // stop
  TIM4_TimeBaseInit(TIM4_PRESCALER_4, TIM4_US_PERIOD);
  TIM4_ClearFlag(TIM4_FLAG_UPDATE);
  TIM4_ITConfig(TIM4_IT_UPDATE, ENABLE);
  
  count = us>>1;
  
  TIM4_Cmd(ENABLE);       // let's go
  
  while(count != 0);
}

//
// Dealy in milliseconds
//
void delay_ms(uint32_t ms)
{
  TIM4_Cmd(DISABLE);       // stop
  TIM4_TimeBaseInit(TIM4_PRESCALER_128, TIM4_PERIOD);
  TIM4_ClearFlag(TIM4_FLAG_UPDATE);
  TIM4_ITConfig(TIM4_IT_UPDATE, ENABLE);
  
  count = ms;
  
  TIM4_Cmd(ENABLE);       // let's go
  
  while(count != 0);
}

// 
// Delay check ???
//
void delay_us_check(uint32_t time)
{
  while (time)
  {
    delay_us(1000);
    time--;
  }
}