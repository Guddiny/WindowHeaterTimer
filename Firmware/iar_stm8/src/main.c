#include "stm8s_conf.h"
#include "stm8s_tim2.h"
#include "delay.h"
#include <intrinsics.h>
#include <stdio.h>

typedef enum {
  MIDDLE_ON,
  LONG_ON,
  ULTRA_LONG_ON,
  WAIT
} SYSTEM_STATE;

#define ULTRA_LONG_ON_INTERVAL      1800000 // 30min
#define LONG_ON_INTERVAL            10000   
#define MIDDLE_ON_INTERVAL          3000

volatile SYSTEM_STATE SysState = WAIT;
static uint32_t counter = 0;
static uint32_t btnfHoldcounter = 0;

//
// Initialize sysytm settings
//
int SystemInit(void)
{
  // ---------- CLK CONFIG -----------------------
  CLK_DeInit();
  
  CLK_SYSCLKConfig(CLK_PRESCALER_CPUDIV1);
  CLK_SYSCLKConfig(CLK_PRESCALER_HSIDIV1); // set 16 MHz for CPU
  
  return 0;
}

//
// External Interrupt PORTB Interrupt routine.
//
//#ifndef EXTI_PORTB_IRQ 
//INTERRUPT_HANDLER(EXTI_PORTB_IRQHandler, 4)
//{ 
//  if(SysState == WAIT)
//  {
//    SysState = LONG_ON; 
//    counter = LONG_ON_INTERVAL;
//  }
//  else 
//  {
//    SysState = WAIT;
//    counter = 0;
//  }
//}
//#endif

void SytemStateHandler()
{
  if(SysState == WAIT)
  {
    if (btnfHoldcounter > 9500) // ~ 10 sec
    {
      SysState = ULTRA_LONG_ON; 
      counter = ULTRA_LONG_ON_INTERVAL;
    }
    else if (btnfHoldcounter > 2700) // ~ 3 sec
    {
      SysState = LONG_ON; 
      counter = LONG_ON_INTERVAL;
    }
    else
    {
      SysState = MIDDLE_ON; 
      counter = MIDDLE_ON_INTERVAL;
    }
  }
  else 
  {
    SysState = WAIT;
    counter = 0;
  }
}

//
// Read button with debounce
//
BitStatus ButtonRead()
{
  BitStatus buttonState = GPIO_ReadInputPin(GPIOB, GPIO_PIN_4);
  if (buttonState == RESET)
  {
    delay_ms(50);
    buttonState = GPIO_ReadInputPin(GPIOB, GPIO_PIN_4);
    if (buttonState == RESET)
    {
      btnfHoldcounter = 0;
      while (GPIO_ReadInputPin(GPIOB, GPIO_PIN_4) == RESET)
      {
        delay_ms(1);
        btnfHoldcounter++;
      }
      return RESET;
    }
  }
  
  return SET;
}

//
// Application entry point
//
void main(void)
{ 
  SystemInit();
  
  GPIO_Init(GPIOB, GPIO_PIN_5, GPIO_MODE_OUT_PP_HIGH_FAST); // LED Green
  GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_FAST); // Relay 
  GPIO_Init(GPIOB, GPIO_PIN_4, GPIO_MODE_IN_FL_NO_IT); // BUTTON
  
  enableInterrupts();
  
  SysState = WAIT;
  
  while (1)
  {
    delay_ms(1);
    
    if(ButtonRead() == RESET)
    {
      SytemStateHandler();
      delay_ms(50);
    }
    
    if(SysState == WAIT)
    {
      GPIO_WriteLow(GPIOC, GPIO_PIN_5);
    } 
    else
    {
      if (counter != 0)
      {
        counter--;
        GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
      }
      else
      {
        SysState = WAIT;
      }
    }
  }
  
}


//------------------------------------------------------------------------------
// Assertions handler
//
#ifdef USE_FULL_ASSERT

/**
* @brief  Reports the name of the source file and the source line number
*   where the assert_param error has occurred.
* @param file: pointer to the source file name
* @param line: assert_param error line source number
* @retval None
*/
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
  ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  
  /* Infinite loop */
  while (1)
  {
  }
}
#endif